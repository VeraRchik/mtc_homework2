package com.company;

public abstract class GeomFigure {
    public abstract double getPerimeter();
    public abstract double getArea();
}
