package com.company;

public class Rhomb extends Polygon implements Signs {
    public Rhomb(int countTop) {
        super(countTop);
    }

    @Override
    public boolean parallelism() {
        double k1 = (arrayX.get(1) - arrayX.get(0)) / (arrayY.get(1) - arrayY.get(0));
        double k2 = (arrayX.get(3) - arrayX.get(2)) / (arrayY.get(3) - arrayY.get(2));
        return k1==k2;
    }

    @Override
    public boolean equality() {
        return Math.sqrt(Math.pow(arrayX.get(1) - arrayX.get(0), 2) + Math.pow(arrayY.get(1) - arrayY.get(0), 2)) == Math.sqrt(Math.pow(arrayX.get(3) - arrayX.get(2), 2) + Math.pow(arrayY.get(3) - arrayY.get(2), 2));
    }

    public boolean signsRhomb(){
        return equality() & parallelism();
    }
}
