package com.company;

import java.sql.Array;
import java.util.ArrayList;

public class Polygon extends GeomFigure implements WithAngles{
    private int countTop;
    private int x;
    private int y;

    ArrayList <Integer> arrayX = new ArrayList();
    ArrayList <Integer> arrayY = new ArrayList();

    public Polygon() {

    }


    public int[] createArrX(){
        return new int[countTop];
    }

    public int[] createArrY() {
        return new int[countTop];
    }

    public void setCountTop(int countTop) {
        this.countTop = countTop;
    }

    public int getCountTop() {
        return countTop;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public Polygon(int countTop) {
        this.countTop = countTop;
    }

    public void setY(int y) {
        this.y = y;
    }


    @Override
    public void printCoordinates() {
        for(int i = 0; i < countTop;i++)
        System.out.println("Координата x: " + arrayX.get(i) + " Координата y: " + arrayY.get(i));
    }

    @Override
    public double getPerimeter() {
        double P=0;

        for ( int i = 0;i<countTop;i++){
            if(i==countTop-1)
                break;
            else {
                P += Math.sqrt(Math.pow(arrayX.get(i + 1) - arrayX.get(i), 2) + Math.pow(arrayY.get(i + 1) - arrayY.get(i), 2));

            }
        }
        P+= Math.sqrt(Math.pow(arrayX.get(countTop-1) - arrayX.get(0),2) + Math.pow(arrayY.get(countTop-1)-arrayY.get(0),2));

        return P;
    }

    @Override
    public double getArea() {
        double p = getPerimeter()/2;
        double S = p;
        for ( int i = 0;i<countTop;i++){
            if(i==countTop-1)
                break;
            else {
                S *= p - Math.sqrt(Math.pow(arrayX.get(i + 1) - arrayX.get(i), 2) + Math.pow(arrayY.get(i + 1) - arrayY.get(i), 2));
            }
        }
        S *= p - Math.sqrt(Math.pow(arrayX.get(countTop-1) - arrayX.get(0),2) + Math.pow(arrayY.get(countTop-1)-arrayY.get(0),2));

        return S;
    }

}
