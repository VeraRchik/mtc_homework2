package com.company;

public class Circle extends GeomFigure{
    private int radius;
    private int center_x = 0;
    private int center_y = 0;
    private String color = "red";

    public Circle(){

    }
    public Circle(String color) {
        this.color = color;
    }


    public Circle(int radius) {
        this.radius = radius;
    }

    public Circle(int radius, int center_x, int center_y, String color) {
        this.radius = radius;
        this.center_x = center_x;
        this.center_y = center_y;
        this.color = color;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getCenter_x() {
        return center_x;
    }

    public void setCenter_x(int center_x) {
        this.center_x = center_x;
    }

    public int getCenter_y() {
        return center_y;
    }

    public void setCenter_y(int center_y) {
        this.center_y = center_y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String colorCircle(String new_color){
        this.color=new_color;
        return color;
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public double getArea() {
        return Math.PI*Math.pow(radius,2);
    }
}
