package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void test1() {
        Circle circle1 = new Circle(3);
        System.out.println(circle1.colorCircle("yellow"));
        System.out.println(circle1.getArea());
        System.out.println(circle1.getPerimeter());
        circle1.setRadius(5);
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getCenter_x() + " " + circle1.getCenter_y());
        circle1.setCenter_x(1);
        circle1.setCenter_y(1);
        System.out.println(circle1.getCenter_x() + " " + circle1.getCenter_y());
        circle1.setColor("pink");
        System.out.println(circle1.getColor());
        Circle circle2 = new Circle("green");
        Circle circle3 = new Circle();
        System.out.println(circle2.getColor());
        System.out.println(circle3.getRadius());
        Circle circle4 = new Circle(2, 0, 0, "violet");
        System.out.print(circle4.getRadius() + " " + circle4.getColor() + " " + circle4.getCenter_x() + circle4.getCenter_y() + " " + circle4.getArea() + " " + circle4.getPerimeter());
        System.out.println();
    }

    public static void test2() {
        Square square1 = new Square("blue");
        square1.printColor();
        System.out.println(square1.colorFigure("red"));
        square1.setCountTop(4);
        for (int i = 0; i < square1.getCountTop(); i++) {
            square1.arrayX.add(i);
            square1.arrayY.add(i + 1);
        }
        square1.printCoordinates();

        Square square2 = new Square();
        System.out.println(square2.getX());
        System.out.println(square2.getY());
        square2.setX(2);
        square2.setY(2);
        System.out.println(square2.getX() + " " + square2.getY());

        Square square3 = new Square(2);
        System.out.println(square3.getSide());

        Square square4 = new Square("grey", 3);
        System.out.println(square4.getNew_color() + " " + square4.getSide());
    }
    public static void test3(){
        Polygon triangle = new Polygon(3);
        System.out.println(triangle.getCountTop());
        System.out.println("Введите стороны треугольника");
        for (int i = 0; i < triangle.getCountTop(); i++) {
            Scanner scanner = new Scanner(System.in);
            triangle.arrayX.add(scanner.nextInt());
            triangle.arrayY.add(scanner.nextInt());
        }
        System.out.println(triangle.getPerimeter());
        System.out.println(triangle.getArea());
        triangle.printCoordinates();
    }
    public static void test4(){
        Rhomb rhomb = new Rhomb(4);
        System.out.println(rhomb.getCountTop());
        System.out.println("Введите стороны ромба");
        for (int i = 0; i < rhomb.getCountTop(); i++) {
            Scanner scanner = new Scanner(System.in);
            rhomb.arrayX.add(scanner.nextInt());
            rhomb.arrayY.add(scanner.nextInt());
        }
        System.out.println(rhomb.signsRhomb());
        System.out.println(rhomb.getPerimeter());
        System.out.println(rhomb.getArea());
    }
    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
      /*  -1
        2
        2
        1
        1
                -2   - данные для теста для нахождения периметра и площади треугольника*/


       /*1
1
1
-1
-1
-1
-1
1 - данные для проверки ромба и квадрата*/
    }
}
