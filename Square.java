package com.company;

public class Square extends Polygon{
    private String new_color;
//    private int countTop = 4;
    private int side = 2;

    @Override
    public int getCountTop() {
        return super.getCountTop();
    }

    public Square(){}

    @Override
    public int[] createArrX() {
        return super.createArrX();
    }

    @Override
    public int[] createArrY() {
        return super.createArrY();
    }

    public Square(String new_color) {
        super();
        this.new_color = new_color;
    }

    public Square(int side) {
        this.side = side;
    }

    public String getNew_color() {
        return new_color;
    }

    public int getSide() {
        return side;
    }

    public Square(String new_color, int side) {
        this.new_color = new_color;
        this.side = side;
    }

    public void printColor(){
        System.out.println(new_color);
    }

    @Override
    public double getPerimeter() {
        return side*4;
    }

    @Override
    public double getArea() {
        return Math.pow(side,2);
    }


    public String colorFigure(String color) {
        this.new_color=color;
        return new_color;
    }


    @Override
    public void printCoordinates() {
        for(int i = 0; i < getCountTop();i++)
            System.out.println("Координата x: " + arrayX.get(i) + " Координата y: " + arrayY.get(i));
    }
}
